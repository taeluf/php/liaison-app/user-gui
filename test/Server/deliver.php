<?php

// print_r($_SERVER);
// exit;
error_log("\n\n-----".$_SERVER['REQUEST_URI']."-----\n\n");

$dir = dirname(__DIR__,2);

require($dir.'/vendor/autoload.php');
require(__DIR__.'/bootstrap.php');

// so i can actually test user login & skirt ip throttling
if (isset($_POST['test_spoof_ip'])){
    $_SERVER['REMOTE_ADDR'] = $_POST['test_spoof_ip'];
} else if (isset($_GET['test_spoof_ip'])){
    $_SERVER['REMOTE_ADDR'] = $_GET['test_spoof_ip'];
}


$lia = new \Lia();  
  
$main = \Lia\Package\Server::main($lia);  
$lia->set('user.base_url', '/user/');
$user_package = new \Lia\Package\Server($lia, 'user', $dir.'/code/');  

$settings = json_decode(file_get_contents(dirname(__DIR__).'/db-env.json'),true);
list("db"=>$db_name,"user"=> $user,"password"=> $password) = $settings;
$pdo = new \PDO("mysql:dbname=$db_name", $user,$password);
// i only want to prepare() the db when i start the server
// or maybe during prepare() on the server tests. yes that
// $tester->prepare();


$lib = new \Tlf\User\Lib($pdo);

$lia->methods['csrf_fails'] = [$lib, 'csrf_fails'];

$lib->config = [
    // 'web_address'=>'http://localhost:'.file_get_contents(__DIR__.'/.phptest-host'),
    'web_address'=>'http://create.localhost',
    'email_from'=>'test@tlf.userlib',
];
$current_user = $lib->user_from_cookie();
if ($current_user===false)$current_user = new \Tlf\User($pdo);

$user_package->public_file_params = [
    'user'=>$current_user,
    'lib'=>$lib
];
$user_package->lib = $lib;


if (isset($_GET['disable_pages'])){
    $lib->disabled_pages = [
        'login',
        'register',
        'reset-password',
        'logout',
        'terms',
    ];
}
  
//comment this line out in step 1  
// require(__DIR__.'/add-route.php');


// $lia->dump();
// exit;

$lia->addRoute('@GET.@POST./csrf-test/',
    function ($route, $response) use ($lib){
        $key = $lib->enable_csrf('csrf-test', 10, '/csrf-test-post/');
        $data = $_SESSION[$key];
        $data['key'] = $key;
        $response->content = json_encode($data);
        $response->useTheme = false;
    }
);
$lia->addRoute('@POST./csrf-test-post/',
    function ($route, $response) use ($lib){

        $response->useTheme = false;
        if ($lib->csrf_is_valid('csrf-test')){
            $response->content = 'csrf post test success';
            return;
        }
        $response->content = 'csrf post test not valid';
    }
);

// force CSRF to pass
if (!isset($_SERVER['HTTP_USER_AGENT'])
    &&!isset($_GET['enable_csrf'])
    &&!isset($_POST['enable_csrf'])
    &&!isset($_POST['agreed_to_terms'])
){
    // print_r($_SERVER);
    // exit;
    // $_SERVER['HTTP_REFERER'] = 'http://localhost';
    // foreach ($prefixes as $p){
        // $post_key = $lib->get_csrf_post_key($p);
        // if ($post_key != '')$lib->valid_sessions[$post_key] = true;
    // }
    foreach ($_POST as $k=>$v){
        if (strpos($k,'-csrf-')!==false)$lib->valid_sessions[$k] = true;
    }
    $prefixes = ['csrf-test', 'request-password', 'complete-password', 'login', 'register'];
    foreach ($prefixes as $p){
        $_POST[$k=$p.'-csrf-force_pass'] = 'forced pass';
        $lib->valid_sessions[$k] = true;
    }

    $_POST['logs_consent'] = 'on';
    $_POST['agreed_to_terms'] = 'on';
}

if ( !isset($_SERVER['HTTP_USER_AGENT'])
    &&!isset($_POST['honey']) ){
    $_POST['honey'] = '1,2,3';
    $_POST['honey_answer'] = password_hash('answer', PASSWORD_DEFAULT);
    $_POST['1'] = '';
    $_POST['2'] = '';
    $_POST['3'] = 'answer';
}

$lia->deliver();

