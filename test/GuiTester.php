<?php

namespace Tlf\User;

class GuiTester extends \Tlf\User\Tester {

    public function onBeforeTest(){
        $this->clear_email();
    }

    public function clear_email(){
        $file = $this->file('test/Server/email-body-out.txt');
        file_put_contents($file, '--no-email--');
    }
    public function get_email(){
        $email = file_get_contents($this->file('test/Server/email-body-out.txt'));
        return $email;
    }

    /** @return a fake ip address */
    public function spoof(string $key){
        $ips = [
            'test.logout'=>'0.0.0.5',
            'post.throttle.login'=>'0.0.0.1',
            'post.throttle.login2'=>'0.0.0.12',
            'post.throttle.ip'=>'0.0.0.13',
            'post.login'=>'0.0.0.2',
            'post.fail.login'=>'0.0.0.3',
            'post.already.loggedin'=>'0.0.0.4',
        ];
        return $ips[$key];
    }

    /**
     * Logs in with the given email & returns the cookie code.
     * @return cookie code from a successful login
     */
    public function login($email){
        $path = '/user/login/';
        $password = 'abc';
        $email = $email;
        $user = $this->get_active_user($email, $password);
        $params = [
            'email'=>$email,
            'password'=>$password,
            'test_spoof_ip'=>$this->spoof(explode('@',$email)[1]),
        ];
        $login_response = $this->curl_post($path, $params);
        $cookie = $login_response['cookies']['taeluf_login']['value'];
        return $cookie;
    }

    /**
     * @param $path the relative page url
     * @param $code the cookie code to send for logged in user
     */
    public function cookie_get($path, $code){
        $ch = curl_init();

        $protocol = $this->cli->get_server_protocol();
        $host = $this->cli->get_server_host();
        $url = $protocol.$host.$path;
        
        curl_setopt_array($ch,
            [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => false,
            // CURLOPT_POSTFIELDS => http_build_query($params),
            // CURLOPT_POSTFIELDS => $params,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTPHEADER=>[
                    "Cookie: taeluf_login=$code"
                ],
            ]
        );

        $response = $body = curl_exec($ch);
        return $response;
    }
}
