<?php

namespace Tlf\User\Test;

class Permissions extends \Tlf\User\Tester {

    /** a user initiated in prepare() so we can easily benchmark JUST checking perms */
    public $timing_user;

    public function prepare(){
        parent::prepare();

        $this->timing_user = $user = $this->get_active_user('reed@role.allow.timing');
        $this->is_false($user->can('blog:delete'));

        $user->add_role('admin.role.timing');

        $lib = new \Tlf\User\Lib($user->pdo);
        $lib->role_allow('admin.role.timing', 'blog:delete');
    }

    public function testPermLists(){
        $user = $this->get_active_user($email='reed@perm.lists',$password= 'pass');

        $lib = new \Tlf\User\Lib($user->pdo);

        $user->add_role('list');
        $lib->role_allow('list', 'one');
        $lib->role_allow('list', 'two');
        $lib->role_allow('list', 'three');


        $user->add_role('list2');
        $lib->role_allow('list2', 'a one');
        $lib->role_allow('list2', 'a two');
        $lib->role_allow('list2', 'a three');

        $user->allow('perm 1');
        $user->allow('perm 2');
        $user->allow('perm 3');
        $user->allow('perm 4');
        $user->allow('perm 5');
        $user->allow('perm 6');
        $user->allow('perm 7');

        $perms = $user->all_nonrole_permissions();
        $roles = $user->all_roles();

        $this->compare_arrays(
            [
                'perm 1',
                'perm 2',
                'perm 3',
                'perm 4',
                'perm 5',
                'perm 6',
                'perm 7',
            ], $perms);

        $this->compare_arrays([
            'list'=>['three','two','one'],
            'list2'=>['a three', 'a two', 'a one'],
        ], $roles);


        echo "\n\nEmail: $email\nPassword: $password";
    }

    public function testRoleDeny(){
        $user = $this->get_active_user('reed@role.deny');
        $this->is_false($user->can('blog:delete'));

        $user->add_role('admin.roledeny');

        $lib = new \Tlf\User\Lib($user->pdo);
        $lib->role_allow('admin.roledeny', 'blog:delete');
        $lib->role_allow('admin.roledeny', 'blog:create');

        $this->is_true($user->can('blog:delete'));

        $lib->role_deny('admin.roledeny', 'blog:delete');
        
        $user = $lib->user_from_email('reed@role.deny');
        $this->is_false($user->can('blog:delete'));
        $this->is_true($user->can('blog:create'));

        $lildb = new \Tlf\LilDb($user->pdo);
        // print_r($lildb->select('role_permission'));
        // print_r($lildb->select('user_role'));
        $this->is_true(count($lildb->select('role_permission', ['role'=>'admin.roledeny']))==1);

    }
    public function testRoleDelete(){
        $user = $this->get_active_user('reed@role.delete');
        $this->is_false($user->can('blog:delete'));

        $user->add_role('admin.roledel');

        $lib = new \Tlf\User\Lib($user->pdo);
        $lib->role_allow('admin.roledel', 'blog:delete');
        $lib->role_allow('admin.roledel', 'blog:create');

        $this->is_true($user->can('blog:delete'));

        $lib->role_delete('admin.roledel');
        
        $user = $this->get_active_user('reed@role.delete');
        $this->is_false($user->can('blog:delete'));
        $this->is_false($user->can('blog:create'));

        $lildb = new \Tlf\LilDb($user->pdo);
        $this->compare([],$lildb->select('role_permission', ['role'=>'admin.roledel']));
        $this->compare([],$lildb->select('user_role', ['role'=>'admin.roledel']));

    }

    public function testRoleAllowTiming(){
        $this->is_true($this->timing_user->can('blog:delete'));
    }
    public function testRoleDenyTiming(){
        $this->is_false($this->timing_user->can('blog:create'));
    }
    public function testHasRole(){
        $user = $this->get_active_user($email = 'reed@role.has');
        $this->is_false($user->has_role('admin'));

        $user->add_role('admin');

        $this->is_false($user->has_role('admin'));
        $user->roles = null;
        $this->is_true($user->has_role('admin'));

        $pdo = $this->pdo();
        $lib = new \Tlf\User\Lib($pdo);
        $user = $lib->user_from_email($email);

        $this->is_true($user->has_role('admin'));
        $this->is_false($user->has_role('guest'));
        
    }

    public function testRoleAllow(){
        $user = $this->get_active_user('reed@role.allow');
        $this->is_false($user->can('blog:delete'));

        $user->add_role('admin.role');

        $lib = new \Tlf\User\Lib($user->pdo);
        $lib->role_allow('admin.role', 'blog:delete');

        $this->is_true($user->can('blog:delete'));
        $this->is_false($user->can('blog:create'));

        $user->allow('blog:delete');
        $this->is_true($user->can('blog:delete'));
        
    }

    public function testDeny(){
        $user = $this->get_active_user('reed@simple.remove');
        $this->is_false($user->can('blog:edit'));
        $user->allow('blog:edit');
        $this->is_true($user->can('blog:edit'));

        $user->deny('blog:edit');
        $this->is_false($user->can('blog:edit'));
    }

    public function testAllow(){
        $user = $this->get_active_user('reed@simple.allow');

        $this->is_false($user->can('blog:create'));

        $user->allow('blog:create');

        $this->is_true($user->can('blog:create'));
    }
}
