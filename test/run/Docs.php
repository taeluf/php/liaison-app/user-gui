<?php

namespace Tlf\User\Test;

class Docs extends \Tlf\User\GuiTester {

    public function prepare(){
        parent::prepare();
        $pdo = $this->pdo();
        $lib = new \Tlf\User\Lib($pdo);
        $user = $lib->user_from_email('reed@document.permissions');
        $user->register('password');
    }

    public function testPermissions(){
        $pdo = $this->pdo();
        $lib = new \Tlf\User\Lib($pdo);
        $user = $lib->user_from_email('reed@document.permissions');
        //user must already be registered!


        // allow a user to create blog posts
        $user->allow('blog:create');
        // allow user to edit a blog post with id = 37
        $user->allow('blog:edit-37');

        $this->is_true( // an assertion with my test lib
            // check if user can create a blog post
            $user->can('blog:create')
        );

        // remove permission
        $user->deny('blog:create');
        $this->is_false( // assertion to ensure the permission was removed
            $user->can('blog:create')
        );

        /////
        // roles
        /////
        
        // add a role to the user
        $user->add_role('admin');

        // add a permission to the role
        $lib->role_allow('admin', 'blog:delete');

        $this->is_true(
            $user->can('blog:delete')
        );

        // remove a specific permission from a role
        $lib->role_deny('admin', 'blog:delete');
        // delete a role alltogether
        $lib->role_delete('admin');
    }

    /**
     * registers a user, completes registration, and logs in
     * assertions are pretty lazy since all this stuff is thoroughly tested by the main server tests.
     */
    public function testServer(){

        // not trying to test everything
        // let's ...
        // let's create a user, complete registration, then login
        $fake_ip = 'test.docserver.ip';
        $email = 'reed@test.docserver';
        $password = 'Abcd1234!@';
        $response = $this->post('/user/register/',
            [   'email'=>$email,
                'email_confirm'=>$email,
                'password'=>$password,
                'password_confirm'=>$password,
                'test_spoof_ip'=>$fake_ip
            ],
            // $files=[],'doc'
        );

        // echo $response;
        // exit;

        $this->str_contains($response, 'An email has been sent to reed@test.docserver. Please check your email to finish registration.');

        $lildb = new \Tlf\LilDb($this->pdo());
        $rows = $lildb->query(
            'SELECT code FROM code c 
                JOIN user u 
                    ON u.id = c.user_id
                WHERE u.email LIKE :email
            ',['email'=>$email]);
        $code = $rows[0]['code'];

        $response = $this->get('/user/complete.registration.'.$code.'/');

        $this->str_contains($response, 'Registration complete!');


        $response = $this->post('/user/login/',
            ['email'=>$email, 'password'=>$password,
                'test_spoof_ip'=>$fake_ip
            ],
            // [], 'doc',
        );

// echo "\n\n\n-----------\n\n";
// echo "\n\n\n-----------\n\n";
// echo "\n\n\n-----------\n\n";
//         echo $response;
// echo "\n\n\n-----------\n\n";

        $rows = $lildb->select('code', ['type'=>'login_cookie']);
        $this->is_true(count($rows)==1);
    }
}
