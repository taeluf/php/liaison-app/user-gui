<?php

namespace Tlf\User\Test;

class Throttle extends \Tlf\User\Tester {


    public function testClearStaleThrottles(){

        $lib = new \Tlf\User\Lib($this->pdo());

        $ip = 'st';

        for ($i=0;$i<100;$i++){
            $lib->add_throttle('stale',$ip,16);
        }

        $remainder = $lib->throttle_remaining('stale', $ip);
        $this->is_true($remainder>2);
        $this->is_true($remainder<17);

        usleep(21*1000);

        $lib->clear_stale_throttles();

        $remainder = $lib->throttle_remaining('stale',$ip);

        $this->compare(0,$remainder);

    }

    public function testDuplicateThrottle(){
        $lib = new \Tlf\User\Lib($this->pdo());

        $ip = 'mt';

        $lib->add_throttle('multi',$ip,3000);
        $lib->add_throttle('multi',$ip,1000);
        $lib->add_throttle('multi',$ip,2000);

        $remaining = $lib->throttle_remaining('multi',$ip);

        $this->is_true(
            $remaining < 3000
        );
        $this->is_true(
            $remaining > 2970
        );

        echo "\nRemaining:$remaining";

    }


    public function testThrottle(){
        $lib = new \Tlf\User\Lib($this->pdo());

        $ip = '123.233.333.444';

        // check if a throttle is pending
        $this->is_true($lib->throttle_remaining('login', $ip)===0);

        $lib->add_throttle('login',$ip, 2000);

        $remaining = $lib->throttle_remaining('login', $ip);
        $this->test("remaining ($remaining)");
        $this->is_true(
             $remaining > 1990
        );
        $this->is_true(
            $remaining < 2001
        );

        $this->test("throttle (117)");
        // return
        $lib->add_throttle('login2', $ip, 117);
        $start = microtime(true);
        $lib->throttle('login2', $ip);
        $end = microtime(true);
        $duration = $end - $start;
        $duration = $duration * 1000;
        $this->is_true($duration >= 107);
        $this->is_true($duration < 127);

        return;

        echo "\ntime passed during throttle: ".$duration;

        $this->test("own usleep(60*1000) throttle remainder test");

        $lib->add_throttle('login4', $ip, 600);
        $u_start = microtime(true);
        usleep(50*1000);
        $u_end = microtime(true);
        $u_duration = $u_end - $u_start;
        $u_duration *= 1000;
        $remainder = $lib->throttle_remaining('login4',$ip);
        $this->is_true($remainder <= 550);
        $this->is_true($remainder > 540);
        echo "\nDuration: $u_duration";
        echo "\nremainder: $remainder";
    }







    //  This was for debugging issues i was having with throttle time.
    //
    //  basically i needed NOW(6) instead of NOW()
    //
    // public function testDebugThrottle(){
    //     $pdo = $this->pdo();
    //     $stmt = $pdo->prepare(
    //         "SELECT NOW(6) as hi"
    //     );
    //
    //     $stmt->execute();
    //     print_r($stmt->fetch());
    //     $stmt->closeCursor();
    //     usleep(60*1000);
    //     $stmt->execute();
    //     print_r($stmt->fetch());
    // }



    //
    //
    // These commented out bits are for benchmarking
    //
    // TLDR; a simple insert takes 3 milliseconds & that's just how it is.
    //
    //
    //



    // public function testThrottleInsertSpeed(){
    //     $pdo = $this->pdo();
    //     $lib = new \Tlf\User\Lib($pdo);
    //
    //     return;
    //     for ($i=0;$i<1000;$i++){
    //         $lib->add_throttle('abc', 'def', 600);
    //     }
    // }
    // public function testThrottleInsertSpeed2(){
    //     $pdo = $this->pdo();
    //     $lib = new \Tlf\User\Lib($pdo);
    //
    //     // for ($i=0;$i<1000;$i++){
    //         $lib->add_throttle('abc', 'def', 600);
    //     // }
    // }
    //
    // public function testThrottleInsertSpeed3(){
    //     $pdo = $this->pdo();
    //     $lib = new \Tlf\User\Lib($pdo);
    //
    //     for ($i=0;$i<5;$i++){
    //         $lib->add_throttle('abc', 'def', 600);
    //     }
    // }
    //
    // public function testRawInsertSpeed(){
    //     $pdo = $this->pdo();
    //     $pdo->exec(
    //         " INSERT INTO permissions (`id`,`user_id`, `action`, `created_at`)
    //             VALUES(999, 998, 'def', '2018-12-31 23:59:59') ;
    //         "
    //     );
    //     print_r($pdo->errorInfo());
    // }
    //
    // public function testRawCalc(){
    //     $stmt = $this->pdo()->query("SELECT TIMESTAMPADD(SECOND, 1.5, NOW(6)) ");
    //     echo $stmt->fetch()[0];
    // }

}
