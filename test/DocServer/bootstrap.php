<?php

require(__DIR__.'/../Server/bootstrap.php');

// so i can actually test user login & skirt ip throttling
if (isset($_POST['test_spoof_ip'])){
    $_SERVER['REMOTE_ADDR'] = $_POST['test_spoof_ip'];
} else if (isset($_GET['test_spoof_ip'])){
    $_SERVER['REMOTE_ADDR'] = $_GET['test_spoof_ip'];
}
