<?php

// the root dir of your server
$dir = dirname(__DIR__,2);

require($dir.'/vendor/autoload.php');
// optional if you have other setup to do
require(__DIR__.'/bootstrap.php');

// $user_lib_dir = $dir.'/vendor/taeluf/user-gui/code/';
$user_lib_dir = $dir.'/code/';

// initialize Liaison
$lia = new \Lia();  
$main = \Lia\Package\Server::main($lia);  



// set the base url for the user pages
$lia->set('user.base_url', '/user/');
// init the user server
$user_package = new \Lia\Package\Server($lia, 'user', $user_lib_dir);  

// load db settings from env file & make pdo
$settings = json_decode(file_get_contents(dirname(__DIR__).'/db-env.json'),true);
$pdo = new \PDO("mysql:dbname=".$settings['db'], $settings['user'], $settings['password']);

// configure the user login library
$lib = new \Tlf\User\Lib($pdo);
$lib->config = [
    'web_address'=>'https://example.com',
    'email_from'=>'notauser@example.com',
];

// uncomment this line, run once, then re-comment this line
// $lib->init_db();

// log the user in
$current_user = $lib->user_from_cookie();
// if there was no cookie, we'll use an unregistered user
if ($current_user===false)$current_user = new \Tlf\User($pdo);

// passes the user & library to user pages (login, register, etc)
$user_package->public_file_params = [
    'user'=>$current_user,
    'lib'=>$lib
];

 

$lia->deliver();

