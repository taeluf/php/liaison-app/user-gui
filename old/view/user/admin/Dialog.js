class Dialog extends Autowire {

    get content(){return this.q('.Body');}
    get cancelBtn(){return this.q('.Header button');}

    onAttach(){
        document.addEventListener('keyup', this.onkeyup.bind(this));
        this.cancelBtn.addEventListener('click', this.hide.bind(this));
    }

    show(html){
        this.content.innerHTML = html;
        this.n.style.display = "flex";
    }
    hide(){
        this.n.style.display = "none";
    }

    onkeyup(event){
        if (event.keyCode==27){
            this.hide();
            event.stopPropagation();
        }
    }

}

Dialog.autowire('.tlf-user.Dialog');
