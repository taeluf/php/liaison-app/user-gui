<?php

//
// What do I need to do with the users registered on the site?
// Delete them / disable them
// add them to roles
// remove them from roles
// add permissions
// remove permissions
// Edit their name / email
// VIEW permissions & roles
//
//
// Okay... So those are all things I need to do. Every single one. It doesn't matter where I start. Doesn't matter which one I do. They ALL have to be done. Allowing editing of email is... a bad idea, probably. But if you have access to the database... you can do whatever you want, soooo...
// Let's start with editing the name

$pdo = $lia->api('tlf.user','getpdo');
$statement = $pdo->prepare(
    <<<SQL
        SELECT users.*, GROUP_CONCAT(roles.name) AS roles FROM users
        LEFT JOIN role_users
            ON role_users.user_id = users.id
        LEFT JOIN roles 
            ON roles.id=role_users.role_id
        GROUP BY users.id
    SQL
);
$statement->execute();
$users = $statement->fetchAll(PDO::FETCH_ASSOC);

// print_r($statement->errorInfo());

?>
<table class="tlf-user AdminTable">
    <thead>
        <th>Email</th>
        <th>First</th>
        <th>Last</th>
        <th>Roles</th>
        <th>Delete</th>
    </thead>
    <tbody>
    <?php
        foreach ($users as $u){
            echo $lia->view('user/admin/UserRow', ['user'=>$u, 'roles'=>$u['roles']]);
        }
    ?>
    </tbody>
</table>

<?=$lia->view('user/admin/Dialog');?>
