<?php

$user = $this->app->getLoggedInUser();

$this->app->addAdministrativeEmail('');
if (!$user->isAdmin($this->app)){
    echo 'The page you were looking for does not exist.';
    $this->showAtUrl("/");
    return;
}

$pagesJSON = file_get_contents(__DIR__.'/../extra/disabled-pages.json');
$disabledPages = json_decode($pagesJSON);

?>

<section>
<h1>User Administration</h1>
<p>This page is for managing user accounts, permissions, roles, and enabling/disabling certain pages (like registration).</p>
</section>

<section>
<h1>Pages</h1>
<p>If you disable the wrong page, you might heck yourself and need to get into the code.</p>
<p>So what will actually go here is... a list of checkboxes. it'll be a form. </p>
<p>So what it will do is get a list of all the views. It will show every view, and for any view that's matched in '$disabledPages', it will uncheck the box</p>
<p>So enabled pages will be checked. Disabled pages will be unchecked.</p>
<?php

    foreach ($disabledPages as $index=>$page){
        echo '<p>'.$page.'</p>';
    }