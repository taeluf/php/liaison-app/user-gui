<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Configurations  
- `dir.public.baseUrl`: default `/user/`, sets the url prefix for this package  
- `Register.requireName`: default `false`, true to require a name be input when registering  
- `Site.url`: defaults to the url of the website this script is running on. Example: `https://some-url.com`. If your login flow happens on a domain different from your actual website, then you'd want to set this.  
- `Site.name`: default `null`, The name of your site. Example: `Generic Recipes` for a site that has recipes.  
- `Support.name`: default `null`, required. Example: `Customer Support`  
- `Support.email`: default `null`, The email address for customers to get help (especially regarding login issues)  
- `Email.noreply`: default `null`, Example: `noreply@some-url.com`, for the reply-to & from address on automated emails  
  
## Database Settings  
- `DB.createIfBefore`: default `0`, If this value is greater than php's `time()` value, then the database will be created.  
- `DB.driver`: default `mysql`,  
- `DB.charset`: default `utf8`,   
- `DB.collation`: default `utf8_unicode_ci`,   
- `DB.host`: default `localhost`, The host to connect to. It may be something like `mysql.yourwebsite.com` if you're in production.  
  
### Required DB Settings  
- `DB.database`: default `null`, The name of the database to store users in  
- `DB.user`: default `null,`, The user to login to the database  
- `DB.password`: default `null`, The password to login to the database  
  
## Debug settings  
- `Debug.sendEmails`: default `true`, false to turn off email sending & print emails in the browser, for testing the user library when an smtp server is not available for `mail()` sending  
- `Debug.throwErrors`: default `false`, set true to re-throw any caught exceptions, instead of providing user-friendly error messages.  
- `Debug.emptyDB`: default `false`, set `true` to delete all rows from every user-related tables on each request.