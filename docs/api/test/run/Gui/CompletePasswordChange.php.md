<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Gui/CompletePasswordChange.php  
  
# class Tlf\User\Test\CompletePasswordChange  
  
  
  
## Constants  
  
## Properties  
- `protected $page_message = 'An email has been sent to %s. Please check your email to finish resetting your password';`   
  
## Methods   
- `protected function get_code($email = 'whatever')`   
- `public function testPostNewPasswordThrottle()`   
- `public function testGetPasswordThrottle()`   
- `public function testPostNewPasswordInactiveUser()`   
- `public function testPostNewPasswordConfirmationInvalid()`   
- `public function testPostNewPasswordCodeInvalid()`   
- `public function testPostNewPasswordEmailInvalid()`   
- `public function testPostNewPasswordInsecure()`   
- `public function testPostNewPasswordWrongEmail()`   
- `public function testPostNewPassword()`   
- `public function testPostInvalidCode()`   
- `public function testCompleteResetFormBeforeRegisterComplete()`   
- `public function testCompleteResetFormBadCode()`   
- `public function testCompleteResetForm()`   
  
