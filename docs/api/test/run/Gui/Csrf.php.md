<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Gui/Csrf.php  
  
# class Tlf\User\Test\Gui\Csrf  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testSubmitResetWithCsrfCheck()`   
- `public function testCsrfSpoofNoPost()`   
- `public function testCsrfSpoof()`   
- `public function testValidateCsrf()`   
- `public function testGenCsrf()`   
  
