<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Gui/RequestPasswordChange.php  
  
# class Tlf\User\Test\RequestPasswordChange  
  
  
  
## Constants  
  
## Properties  
- `protected $page_message = 'An email has been sent to %s. Please check your email to finish resetting your password';`   
  
## Methods   
- `public function testRegisteredNotActivatedUserResetPassword()`   
- `public function testSubmitReset()`   
- `public function testUserNotExists()`   
- `public function testViewResetForm()`   
  
