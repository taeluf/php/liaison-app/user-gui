<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/UserBackend.php  
  
# class Tlf\User\Test\UserBackend  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testPasswordLogin2()`   
- `public function testValidatePassword()`   
- `public function testLogout()`   
- `public function testGetExistingUser()`   
- `public function testPasswordReset()`   
- `public function testPasswordResetExpiration()`   
- `public function testRegistrationExpiration()`   
- `public function testCookieExpiry()`   
- `public function testCookieLogin()`   
- `public function testPasswordLogin()`   
- `public function testPasswordHashing()`   
- `public function testRegisterUser()`   
- `public function testInitDb()`   
  
