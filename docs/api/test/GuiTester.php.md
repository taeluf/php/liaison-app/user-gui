<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/GuiTester.php  
  
# class Tlf\User\GuiTester  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function onBeforeTest()`   
- `public function clear_email()`   
- `public function get_email()`   
- `public function spoof(string $key)`   
- `public function login($email)` Logs in with the given email & returns the cookie code.  
- `public function cookie_get($path, $code)`   
  
