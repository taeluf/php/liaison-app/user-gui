<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/BlackHole.php  
  
# class Tlf\User\BlackHole  
  
  
  
## Constants  
  
## Properties  
- `public $state = false;`   
- `public $validation = null;`   
  
## Methods   
- `public function __construct($validation)`   
- `public function __call($method, $args)`   
  
