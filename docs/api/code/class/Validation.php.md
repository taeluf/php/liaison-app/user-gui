<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/class/Validation.php  
  
# class Tlf\User\Validation  
protected functions must return true/false & when calling them, pass an error message as the first arg & an optional callable as the second arg.  
  
public functions are called directly and should return `$this`, usually.  
  
  
## Constants  
  
## Properties  
- `public $args = [];`   
- `public $lib;`   
- `public $user;`   
- `public $package;`   
- `public $data = [];` either POST or GET, depending which request method was used  
- `public $state = true;`   
- `public string $code_user_email;`   
- `public object $post_user_from_email;`   
- `public array $messages = [  
        'csrf.invalid' => 'Error: CSRF Token invalid. It appears the form was submitted from a different site.',  
    ];`   
  
## Methods   
- `public function __construct($args)`   
- `public function log(string $action, string $email=null, $force_consentfalse)` Log the given action, as long as consent has been given.   
  
- `public function enable_csrf(...$args)`   
- `public function check_csrf(string $key_prefix='', string $message)`   
- `public function show_message(string $message)`   
- `public function show_error(string $message)`   
- `public function goto($url)`   
- `public function throttle($key, $value, $length=5000)`   
- `public function show_csrf_form(string $form_name, string $csrf_prefix, int $csrf_minutes = 30, string $csrf_url'')`   
- `public function show_form($form_name)`   
- `public function set_new_password(string $error='internal error: could not set new password')`   
- `protected function is_get()`   
- `protected function is_post()`   
- `protected function is_logged_out()`   
- `protected function code_is_valid()`   
- `protected function code_user_is_active()`   
- `protected function code_email_matches_post_email()`   
- `protected function post_email_is_valid()`   
- `protected function post_password_is_valid()`   
- `protected function post_password_matches_confirm_password()`   
- `protected function post_email_account_exists()`   
- `protected function post_email_account_notexists()`   
- `protected function post_email_account_active()`   
- `protected function post_email_confirmed()`   
- `protected function post_login()`   
- `protected function agrees_to_terms()`   
- `protected function check_honey()`   
- `public function __call($method, $args)`   
  
