<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/core/oldUserSystem.php  
  
# class TLF\UserSystem  
Functions at a sudo level, thus can manage permissions for any user, manage roles, and probably other stuff???  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function onSelf_PackageSetup($event,$url)`   
- `protected function activeUser()`   
- `public function addRole($user, $role)`   
- `public function addRolePermission($role, $permission, $with=null)`   
- `public function addUserPermission($user, $permission, $with=null)`   
- `public function role($name)`   
- `public function getUser($byKey='email||id', $withValue)`   
- `public function setDBCredentials($dbName,$userName,$password,$extra=[])`   
- `function sendActivationEmail($user,$activation)`   
- `protected function passwordFailsRequirements($password)`   
- `protected function sendMail($email,$subject,$message,$name = 'User')`   
- `public function links(...$files)`   
- `public function tryPasswordReset($data)`   
- `public function tryActivation($data)`   
  
