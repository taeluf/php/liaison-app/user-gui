<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/old/Loader.php  
  
# class ROF\User\Loader  
  
  
  
## Constants  
  
## Properties  
- `protected $loggedInUser;`   
  
## Methods   
- `public function createTables()`   
- `public function getFromId($userId)`   
- `public function getPepper()`   
- `public function getLoggedInUserId()`   
- `public function getLoggedInUser()`   
- `public function tryLogin()`   
- `public function tryLogout()`   
- `public function getCookie()`   
- `public function getRandomString()`   
  
