<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/old/User.php  
  
# class useROF\User\User  
The user object. It will be instantiated and have data filled into it from the Loader class, as the Loader class loads data from the DB  
Stores constants for error code returns (will likely deprecate)  
  
  
## Constants  
- `const LOGIN_SUCCESS = 1000;`   
- `const LOGIN_SUCCESS_CHANGE_PASSWORD = 1001;`   
- `const LOGIN_NO_EMAIL = 1002;`   
- `const LOGIN_NO_PASSWORD = 1003;`   
- `const LOGIN_WRONG_PASSWORD = 1004;`   
- `const LOGIN_UNKNOWN_ERROR = 1005;`   
- `const LOGIN_MULTIPLE_ACCOUNTS = 1006;`   
- `const RESET_FAILED_MAIL = 1050;`   
- `const RESET_SUCCESS = 1051;`   
- `const RESET_FAILED_SET = 1052;`   
- `const RESET_FAILED_REMOVAL = 1053;`   
- `const RESET_NO_ACCOUNT = 1054;`   
- `const RESET_NO_EMAIL = 1055;`   
- `const PASS_SUCCESS = 1100;`   
- `const PASS_FAILED_MAIL = 1101;`   
- `const PASS_FAILED_SET = 1102;`   
- `const PASS_FAILED_DELETE = 1103;`   
- `const PASS_TOO_WEAK = 1104;`   
- `const PASS_MISMATCH = 1105;`   
- `const PASS_INCORRECT = 1106;`   
- `const PASS_INVALID_REQUEST = 1107;`   
- `const REG_NO_EMAIL = 1150;`   
- `const REG_NOT_AN_EMAIL = 1151;`   
- `const REG_EMAIL_MISMATCH = 1152;`   
- `const REG_ACCOUNT_EXISTS = 1153;`   
- `const REG_FAILED_CREATE = 1154;`   
- `const REG_SUCCESS = 1155;`   
- `const REG_FAILED_MAIL = 1156;`   
- `const REG_FAILED_PWD = 1157;`   
  
## Properties  
- `public static $messages = [  
        \ROF\UserLOGIN_NO_EMAIL => "No email was submitted.",  
        \ROF\UserLOGIN_NO_PASSWORD => "No password was submitted.",  
        \ROF\UserLOGIN_WRONG_PASSWORD => "The email or password is incorrect.",  
        \ROF\UserLOGIN_UNKNOWN_ERROR => "There was an error logging you in. Please try again or contact support.",  
        \ROF\UserLOGIN_MULTIPLE_ACCOUNTS => "There was a system error, and you can not be logged in. Please contact support.",  
        \ROF\UserLOGIN_SUCCESS_CHANGE_PASSWORD => "You were logged in, but your password expires soon. Please change it.",  
        \ROF\UserLOGIN_SUCCESS => "You were logged in successfully.",  
          
        \ROF\UserRESET_FAILED_MAIL => "The password reset email failed to send. Please reset your password again.",  
        \ROF\UserRESET_SUCCESS => "Please check your email to finish resetting your password",  
        \ROF\UserRESET_FAILED_SET => "Your password could not be reset due to a system error. Error: RFS",  
        \ROF\UserRESET_FAILED_REMOVAL => "Your password could not be reset due to a system error. Error: RFR",  
        \ROF\UserRESET_NO_ACCOUNT => "There is no account associated with your email address.",  
        \ROF\UserRESET_NO_EMAIL => "Please enter a valid email address.",  
          
        \ROF\UserPASS_SUCCESS => "Your password was changed successfully. You will receive a confirmation email.",  
        \ROF\UserPASS_FAILED_MAIL => "Your password was changed, but the email confirmation failed to send.",  
        \ROF\UserPASS_FAILED_SET => "Your password could not be changed due to a system error. Error: PFS",  
        \ROF\UserPASS_FAILED_DELETE => "Your password could not be changed due to a system error. Error: PFD",  
        \ROF\UserPASS_TOO_WEAK => "Your attempted password does not meet the minimum password requirements.",  
        \ROF\UserPASS_MISMATCH => "The new passwords you entered did not match.",  
        \ROF\UserPASS_INCORRECT => "The current password submitted was not correct.",  
        \ROF\UserPASS_INVALID_REQUEST => "All fields must be filled out. Please try again.",  
        
        \ROF\UserREG_NO_EMAIL => "An email address needs to be entered.",  
        \ROF\UserREG_NOT_AN_EMAIL => "A valid email address needs to be entered.",  
        \ROF\UserREG_EMAIL_MISMATCH => "The email entered does not match the confirmation.",  
        \ROF\UserREG_ACCOUNT_EXISTS => "There is already an account associated with this address. Try resetting your password.",  
        \ROF\UserREG_FAILED_CREATE => "There was a system error, so you were not registered.",  
        \ROF\UserREG_SUCCESS => "Please check your email to complete regitration.",  
        \ROF\UserREG_FAILED_MAIL => "Your account was created, but there was an error. "  
                                        ."Please reset your password to complete registration. Error: RFM",  
        \ROF\UserREG_FAILED_PWD => "Your account was created, but there was an error. "  
                                     ."Please reset your password to complete registration. Error: RFP",  
  
      ];` User-friendly messages for each of the error codes  
- `public $userId;` The user id in the database. Currently an auto-incrementing integer. SHOULD move to an Alpha Numeric UUID...  
- `public $userName = NULL;` NOT USED. Eventually, the publicly displayed user name for the user.  
- `public $realName;` The real name of the user. Not to be shared publicly  
- `public $email;` The email address of the user. Not to be shared publicly  
- `public $passwordExpiration;` a UNIX timestamp (in seconds) indicating when the password expires  
  
## Methods   
- `public function __construct($userId,$userName,$realName,$email,$passwordExpiration)` Construct a User. Generally will be created by the Loader, as it should be filled by database-data.  
  
