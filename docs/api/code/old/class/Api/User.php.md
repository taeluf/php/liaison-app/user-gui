<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/class/Api/User.php  
  
# class Api\User  
  
  
  
## Constants  
  
## Properties  
- `protected $lia;`   
  
## Methods   
- `public function __construct($lia)`   
- `public function apiArgs($data)`   
- `public function apiUpdateArgs($model, $data)`   
- `public function apiUpdate($model, $column, $data)`   
- `public function apiDelete($model, $data)`   
- `public function apiForm($model, $data)`   
  
