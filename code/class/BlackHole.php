<?php

namespace Tlf\User;

class BlackHole {

    public $state = false;
    public $validation = null;

    public function __construct($validation){
        $this->validation = $validation;
    }

    public function __call($method, $args){
        return $this;
    }
}
