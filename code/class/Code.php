<?php

namespace Tlf\User;

class Code {

    const PASSWORD_RESET = "password_reset";
    const REGISTRATION = "registration";
    const LOGIN_COOKIE = "login_cookie";
}
