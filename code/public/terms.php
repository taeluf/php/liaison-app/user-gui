<?php
if ($lib->page_is_disabled('terms'))return;
?>
<h1>Terms & Conditions for Users</h1>
<p>These terms apply to the user registration & login system on this site and do not apply to the rest of this site. Furthermore, this library is open source. These terms apply to the default setup of this user login system. If this website does not adhere to these terms and conditions, the website owner and/or developers shall be responsible, not the developers of this open source user login system.</p>

<h2>Your Data</h2>
<h3>What We Collect</h3>
<ul>
<li>Email address when you register</li>
<li>Encrypted copy of your password when you register or set a new password.</li>
<li>Your IP address, email, and user agent when you login, register, reset a password, or visit any page where a code is being validated (such as when resetting a password or completing registration)</li>
<li>As new features & security measures are added, we may log your IP address in conjunction with those features</li>
</ul>

<h3>How We Use your Data</h3>
<ul>
<li>Email: Identify you as a unique user.</li>
<li>Password (encrypted copy): To protect your account.</li>
<li>IP Address, user agent & email: for throttling to help prevent hacking, and for security logs. Security logs are shown to users. So if you try to login with someone else's email address, they will see your IP address in their security logs. Some IP Logging is NOT tied to a user account, and those IP addresses can only be viewed by the website administrators or anyone with access to the website's database. User-specific IP Addresses can also be viewed by website administrators & those with access to the website database. </li>
</ul>

<h3>Risks of giving us your data</h3>
<p>Hacking is always a risk when using the internet. We have security measures in place to prevent your data from being accessed by anyone you have not authorized to access it. However, there may be flaws in our system. There may be flaws in the server this code is run on. There may be other methods with which one could gain access to your account or information, such as social engineering or person in the middle attacks.</p>

<h2>Liability</h2>
<p><b>You are liable</b> for the information you give us & we will not be held liable if this information is stolen, leaked, or otherwise accessed without your consent. Any liability which might fall on us (the developers of this user login system) instead fall on the owner or developers of the website this library is running on. The <b>website</b> is liable for the software they use, including this library.</p>

<h2>Legal Stuff</h2>
<p>If any of these terms are found not to be legally valid, all other terms present shall remain in effect.</p>

<p>These terms &amp; conditions may change. If they change, it is the responsibility of the website owner to properly inform you of changes and get your consent for the changes.</p>
