<?php

if ($lib->page_is_disabled('logout'))return;

// paths:
// 1. user is logged in. log them out & display this page
// 2. user is NOT logged in. redirect to the login page
// 3. user is logged in. logout fails (database error or something)

if (!$user->is_logged_in()){
    $package->goto('/login/');
    return;
}

$old_code = $user->logout();

$no_user = $lib->user_from_cookie($old_code);
if (is_object($no_user) || $no_user !== false){
    echo "There was an error logging you out. Try refreshing this page.";
    return;
}

$class = $lib->user_class;
$guest_user = new $class($lib->pdo);
$lia->addons['view']->globalArgs['user'] = $guest_user;
$package->user = $guest_user;

?>

<h1>Logged Out</h1>
<p>You have been logged out. Have a great day!</p>
<?php
echo $lia->view('user/Links', ['links'=>['login','register','reset-password','help']]);
