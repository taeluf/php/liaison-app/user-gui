<?php
if (!$user->is_logged_in()){

    echo '<h1>You are not logged in</h1>';

    echo $lia->view('user/Links',['links'=>['login', 'register','reset.password','help']]);
    return;
}

?>
<h1>User Profile for <?=$user->email?></h1>
<p>Please forgive the barebones nature of this page. This user login software is still under development.</p>

<h2>Stuff</h2>
<a href="<?=$package->url('/reset-password/')?>"><button>Set new password</button></a><br><br>
<a href="<?=$package->url('/terms/')?>"><button>Terms &amp; Conditions</button></a><br><br>
<a href="<?=$package->url('/logout/')?>"><button>Logout</button></a><br><br>

<h2>Roles you have (and their permissions)</h2>
<?php
foreach ($user->all_roles() as $role=>$permissions){
    echo "\n<h3>$role</h3>\n<ul>";

    foreach ($permissions as $name){ 
        echo "\n    <li>$name</li>";
    }
    echo "\n</ul>";
}
?>

<h2>Permissions you have</h2>
<?php
echo "\n<ul>";
foreach ($user->all_nonrole_permissions() as $name){
    echo "\n    <li>$name</li>";
}
echo "\n</ul>";
?>

<h2>Security Logs</h2>
<p>History of account activity (login attempts, emails sent to you, registration, etc). We currently only show the last 100 entries and the timezone may not be the same as yours.</p>
<table border=1 style="border-collapse:collapse;"><thead><tr><th>Action</th><th>IP</th><th>User Agent</th><th>Time/Date</th></thead>
<tbody>
<?php foreach ($user->security_logs() as $index=>$entry): ?>
<tr>
    <td><?=$entry['action']?></td>
    <td><?=$entry['ip']?></td>
    <td><?=$entry['user_agent']?></td>
    <td><?=$entry['created_at']?></td>
<?php endforeach; ?>
</tbody>
</table>
